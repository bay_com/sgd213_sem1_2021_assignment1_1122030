﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchingBytes;

/// <summary>
/// Pickup is an object aimed at passing weapon functionality to player objects. Depending on
/// the specified weaponType, the Pickup will tell the player object what object it should now
/// use as it's weapon.
/// </summary>
public class Pickup : MonoBehaviour
{
    [SerializeField]
    private PickupType pickupType;
    
    [SerializeField]
    public WeaponType weaponType;

    // If game object collides with the player, then destroy game object.
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            GameObject player = col.gameObject;

            switch (pickupType)
        {
            case PickupType.weapon:
                HandlePlayerPickup(player);
                break;
            case PickupType.health:
                HandlePlayerHeal(player);
                break;
        }
            EasyObjectPool.instance.ReturnObjectToPool(this.gameObject);
        }
    }

    /// <summary>
    /// HandlePlayerPickup handles all of the actions after a player has been collided with.
    /// It grabs the IWeapon component from the player, transfers all information to a
    /// new IWeapon (based on the provided weaponType).
    /// </summary>
    /// <param name="player"></param>

    private void HandlePlayerPickup(GameObject player)
    {
        // get the playerInput from the player
        PlayerInput playerInput = player.GetComponent<PlayerInput>();
        // handle a case where the player doesnt have a PlayerInput
        if (playerInput == null) {
            Debug.LogError("Player doesn't have a PlayerInput component.");
            return;
        } else {
            // tell the playerInput to SwapWeapon based on our weaponType
            playerInput.SwapWeapon(weaponType);
        }
    }

    private void HandlePlayerHeal(GameObject player)
    {
        player.GetComponent<IHealth>().Heal(20);
    }
}  

// A public enum with the weapon types.
public enum WeaponType { machineGun, tripleShot, none }

public enum PickupType { weapon, health}
