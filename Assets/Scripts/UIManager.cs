﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    [SerializeField]
    private Slider sldHealth;

    // Start is called before the first frame update
    void Start()
    {
        if (instance != null) {
            Debug.LogError("There is more than one UIManager in the scene, this will break the Singleton pattern.");
        }
        instance = this;
    }

    /// <summary>
    /// UpdateHealthSlider handles the updates between the players health and what the slider displays.
    /// </summary>
    /// <param name="percentage">The percentage that the player health bar is filled</param>
    public void UpdateHealthSlider(float percentage) {
        sldHealth.value = percentage;
    }
}
