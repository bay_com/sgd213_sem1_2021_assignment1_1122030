﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchingBytes;


/// <summary>
/// DamageOnCollision gives the attached object a collision behaviour.
/// A damage value 'damageToDeal' will be applied to the other object.
/// </summary>

public class DamageOnCollision : DetectCollisionBase
{
    [SerializeField]
    private int damageToDeal;

    protected override void ProcessCollision(GameObject other)
    {
        base.ProcessCollision(other);
        if (other.GetComponent<IHealth>() != null) {
            other.GetComponent<IHealth>().TakeDamage(damageToDeal);
        } else {
            Debug.Log(other.name + " does not have an IHealth component");
        }

        EasyObjectPool.instance.ReturnObjectToPool(this.gameObject);
    }
}
