﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchingBytes;

public class PlayerHealth : MonoBehaviour, IHealth
{

    [SerializeField]
    private GameObject explosionParticleSystem;

    public Score Score;

    public GameOverScreen gameOverScreen;

    [SerializeField]
    protected int currentHealth;
    public int CurrentHealth { get { return currentHealth; } }

    [SerializeField]
    protected int maxHealth;
    public int MaxHealth { get { return maxHealth; } }

    public bool dead = false;

    void Start()
    {
        currentHealth = maxHealth;
    }

    // <summary>
    /// Heal handles the functionality of receiving health
    /// </summary>
    /// <param name="healingAmount">The amount of health to gain, this value should be positive</param>
    public void Heal(int healingAmount)
    {
        currentHealth += healingAmount;
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

        UIManager.instance.UpdateHealthSlider((float)currentHealth / (float)maxHealth);
    }

    /// <summary>
    /// TakeDamage handles the functionality for taking damage
    /// </summary>
    /// <param name="damageAmount">The amount of damage to lose, this value should be positive</param>
    public void TakeDamage(int damageAmount)
    {
        currentHealth -= damageAmount;

        UIManager.instance.UpdateHealthSlider((float)currentHealth / (float)maxHealth);

        if (currentHealth <= 0)
        {
            currentHealth = 0;
            Die();
        }
    }

    /// <summary>
    /// Handles all functionality related to when health reaches or goes below zero, should perform all necessary cleanup.
    /// </summary>
    public void Die()
    {
        StartCoroutine(WaitBeforeGameOver());
    }

    private IEnumerator WaitBeforeGameOver()
    {  
        Instantiate(explosionParticleSystem, this.transform.position, this.transform.rotation);
        GetComponent<Renderer>().enabled = !GetComponent<Renderer>().enabled;
        yield return new WaitForSeconds(2);
        gameOverScreen.Setup();
        EasyObjectPool.instance.ReturnObjectToPool(this.gameObject);
        Time.timeScale = 0f;
    }
}
