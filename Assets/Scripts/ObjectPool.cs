﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public interface IUsePool
{
    void SetPool(ObjectPool givenObjectPool);
}

public class ObjectPool : MonoBehaviour
{

    public GameObject Pooledtype = null;
    public int Growamount = 200;
    public bool Allowgrow = false;

    private LinkedList<GameObject> Activelist = new LinkedList<GameObject>();
    private Queue<GameObject> Inactivequeue = new Queue<GameObject>();

    public void Start()
    {
        growPool(Growamount);
    }

    private void growPool(int Num)
    {
		for (int I = 0; I < Num; I++)
        {
            GameObject Addedobject = Instantiate(Pooledtype);
            Addedobject.SetActive(false);
            Inactivequeue.Enqueue(Addedobject);

            // On the newly instantiated GameObject
            // Find each Component which wants to know about the Pool (IUsePool)
            // (i.e. any component which would otherwise Destroy the GameObject)
            // And tell it about the Pool
            List<IUsePool> Poolwantingcomponents = new List<IUsePool>();
            Addedobject.GetComponents<IUsePool>(Poolwantingcomponents);
            Poolwantingcomponents.ForEach(Component => Component.SetPool(this));
        }
    }

    // Gets and object from the pool.
    // Grows the pool if necessary
    public GameObject GetObject(Vector3 Position, Quaternion Rotation)
    {
        if (Inactivequeue.Count == 0)
        {
            if (Allowgrow)
            {
                growPool(Growamount);
            }
            else
            {
                return null;
            }
		}

		return (GameObject)Instantiate(Pooledtype, Position, Rotation);
    }

    public void returnObject(GameObject poolObject)
    {

    }
}
