﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchingBytes;

public class WeaponMachineGun : WeaponBase {

    /// <summary>
    /// Shoot will spawn a new bullet, provided enough time has passed compared to our fireDelay.
    /// </summary>

    public string poolName;
	List<GameObject> goList = new List<GameObject>();
    
    public override void Shoot() {
        // get the current time
        float currentTime = Time.time;

        fireDelay = Random.Range(minimumFireDelay, maximumFireDelay);

        // if enough time has passed since our last shot compared to our fireDelay, spawn our bullet
        if (currentTime - lastFiredTime > fireDelay) {
            // create our bullet
            //GameObject newBullet = Instantiate(bullet, bulletSpawnPoint.position, transform.rotation);

            GameObject go = EasyObjectPool.instance.GetObjectFromPool(poolName, bulletSpawnPoint.position, transform.rotation);
            if(go) {
			    goList.Add(go);
		    }

            // update our shooting state
            lastFiredTime = currentTime;
        }
    }
}
