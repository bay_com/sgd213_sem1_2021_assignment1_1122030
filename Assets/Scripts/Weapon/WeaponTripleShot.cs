﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarchingBytes;

public class WeaponTripleShot : WeaponBase {

    /// <summary>
    /// Shoot will spawn a three bullets, provided enough time has passed compared to our fireDelay.
    /// </summary>

    public string poolName;
	List<GameObject> goList = new List<GameObject>();
    public float direction = 0.5f;

    public override void Shoot() {
        // get the current time
        float currentTime = Time.time;

        print("Shoot triple shot");
        // if enough time has passed since our last shot compared to our fireDelay, spawn our bullet

        fireDelay = Random.Range(minimumFireDelay, maximumFireDelay);

        if (currentTime - lastFiredTime > fireDelay) {

            // create 3 bullets
            for (int i = 0; i < 3; i++) {
                // create our bullet
                GameObject go = EasyObjectPool.instance.GetObjectFromPool(poolName, bulletSpawnPoint.position, transform.rotation);
                if(go) {
			        goList.Add(go);
		        }
                // set their direction
                go.GetComponent<MoveConstantly>().Direction = new Vector2(direction + 0.5f * i, direction);
            }


            // update our shooting state
            lastFiredTime = currentTime;
        }
    }
}
