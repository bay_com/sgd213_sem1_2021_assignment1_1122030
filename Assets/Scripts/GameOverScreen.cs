using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;


public class GameOverScreen : MonoBehaviour
{

    public TextMeshProUGUI scoreText;

    public Score playerScore;

    /// <summary>
    /// When Setup is called, the game initiates a sequence to display the game over screen
    /// with the updated score.
    /// </summary>

    public void Setup()
    {
        gameObject.SetActive(true);
        playerScore = FindObjectOfType<Score>();
        int newScore = playerScore.score;
        scoreText.text = "Score: " + newScore.ToString(); 
    }
}
