﻿using UnityEngine;
using MarchingBytes;
using System.Collections;
using System.Collections.Generic;

public class SpawnOverTimeScript : MonoBehaviour
{

    public string poolName;
	List<GameObject> goList = new List<GameObject>();

    // Delay before starting to spawn
    [SerializeField]
    private float initialDelay = 2f;

    // Delay between spawns
    [SerializeField]
    private float minimumSpawnDelay = 1f;

    [SerializeField]
    private float maximumSpawnDelay = 5f;

    private float spawnDelay;

    private Renderer ourRenderer;

    // Use this for initialization
    void Start()
    {
        ourRenderer = GetComponent<Renderer>();

        // Stop our Spawner from being visible!
        ourRenderer.enabled = false;

        // Call the given function after initialDelay seconds, 
        // and then repeatedly call it after spawnDelay seconds.

        spawnDelay = Random.Range(minimumSpawnDelay, maximumSpawnDelay);
        InvokeRepeating("Spawn", initialDelay, spawnDelay);
    }


    /// <summary>
    /// Spawn handles the random spawning of the enemies.
    /// Enemies are spawned at a random location within the bounds of the spawn box.
    /// </summary>  

    void Spawn()
    {
        float X = transform.position.x - ourRenderer.bounds.size.x / 2;
        float XX = transform.position.x + ourRenderer.bounds.size.x / 2;

        // Randomly pick a point within the spawn object
        Vector2 SPAWNLOC = new Vector2(Random.Range(X, XX), transform.position.y);

        // Spawn the object at the 'spawnLocation' position
		
        GameObject go = EasyObjectPool.instance.GetObjectFromPool(poolName, SPAWNLOC, Quaternion.identity);
        if(go) {
			goList.Add(go);
		}
    }
}
