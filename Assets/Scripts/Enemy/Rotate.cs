﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Rotate allows the attached object to spin at a specified speed.
/// </summary>
public class Rotate : MonoBehaviour
{
    [SerializeField]
    private float maximumSpinSpeed = 200;

    void Start()
    {
        GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-maximumSpinSpeed, maximumSpinSpeed);    // Enemy spins at a rate between a selected paramater.
    }
}
