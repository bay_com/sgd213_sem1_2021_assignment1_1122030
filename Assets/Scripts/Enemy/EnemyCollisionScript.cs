﻿using UnityEngine;
using System.Collections;

public class EnemyCollisionScript : MonoBehaviour, IUsePool
{
    private static Score score = null;

    [SerializeField]
    private Points points;

    private ObjectPool objectPool;

    [SerializeField]
    private GameObject explosionParticleSystem; // Attach the particle system.



    // A public enum is created for the different enemy types.
    public enum Points
    {
        asteroid, enemySpaceship, enemyBoss, enemyProjectile
    }

    public void Start()
    {

        if (score == null)
        {
            score = Object.FindObjectOfType<Score>();
        }
    }

    public void SetPool(ObjectPool givenObjectPool)
    {
        objectPool = givenObjectPool;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {

        /*
        Enemies are only destroyed by PlayerProjectile.
        A different score is added depending on what type of enemy the player destroys.
        */

        if (other.tag == "PlayerProjectile")
        {

        switch (points)
        {
            case Points.asteroid:
                score.AddScore(1);
                break;
            case Points.enemySpaceship:
                score.AddScore(2);
                break;
            case Points.enemyBoss:
                score.AddScore(10);
                break;
        }

            if (objectPool != null)
            {
                objectPool.returnObject(gameObject);
            }

            // When the enemy is shot, it gets destroyed and an instance of the explosion particle system is spawned.
            else
            {
                Destroy(gameObject);
                Instantiate(explosionParticleSystem, this.transform.position, this.transform.rotation);
            }
        }
    }
}
