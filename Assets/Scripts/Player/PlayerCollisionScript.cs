using UnityEngine;
using System.Collections;

public class PlayerCollisionScript : MonoBehaviour
{

    [SerializeField]
    private GameObject explosionParticleSystem;

    public GameOverScreen gameOverScreen;

    public Score Score;

    // If an enemy or enemy projectile eneters the player's collision zone, a sub routine is called.
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy" || other.tag == "EnemyProjectile")
        {
            //StartCoroutine(WaitBeforeGameOver());
        }
    }

    /*
    The particle system is spawned and the player is destoryed.
    2 seconds later the game over screen is shown.
    */

    private IEnumerator WaitBeforeGameOver()
    {  
        Instantiate(explosionParticleSystem, this.transform.position, this.transform.rotation);
        GetComponent<Renderer>().enabled = !GetComponent<Renderer>().enabled;
        yield return new WaitForSeconds(2);
        gameOverScreen.Setup();
        Time.timeScale = 0f; 
    }
}
