﻿using UnityEngine;
using System.Collections;

public class PlayerProjectileCollisionScript : MonoBehaviour, IUsePool
{
    private ObjectPool objectPool;

    public void SetPool(ObjectPool givenObjectPool)
    {
        objectPool = givenObjectPool;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        // PlayerProjectiles are destroyed by non-player, non-PlayerProjectile objects
        if (other.tag != "Player" && other.tag != "PlayerProjectile")
        {
            if (objectPool != null)
            {
                objectPool.returnObject(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
