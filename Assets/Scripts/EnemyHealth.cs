using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MarchingBytes;


public class EnemyHealth : MonoBehaviour, IHealth
{

    private static Score score = null;

    [SerializeField]
    private Points points;

    [SerializeField]
    private GameObject explosionParticleSystem;

    [SerializeField]
    protected int currentHealth;
    public int CurrentHealth { get { return currentHealth; } }

    [SerializeField]
    protected int maxHealth;
    public int MaxHealth { get { return maxHealth; } }

    [SerializeField]
    public Slider sldEnemyHealth;

    public enum Points
    {
        asteroid, enemySpaceship, enemyBoss
    }

    void Start()
    {
        currentHealth = maxHealth;
        score = Object.FindObjectOfType<Score>();
    }

    // <summary>
    /// Heal handles the functionality of receiving health
    /// </summary>
    /// <param name="healingAmount">The amount of health to gain, this value should be positive</param>
    public void Heal(int healingAmount)
    {
        currentHealth += healingAmount;
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

        sldEnemyHealth.value = (float)currentHealth / (float)maxHealth;
    }

    /// <summary>
    /// TakeDamage handles the functionality for taking damage
    /// </summary>
    /// <param name="damageAmount">The amount of damage to lose, this value should be positive</param>
    public void TakeDamage(int damageAmount)
    {
        currentHealth -= damageAmount;

        if (currentHealth <= 0)
        {
            currentHealth = 0;
            Die();
        }
        
        if (points != Points.asteroid)
        {
            sldEnemyHealth.value = (float)currentHealth / (float)maxHealth;
        }
    }

    /// <summary>
    /// Handles all functionality related to when health reaches or goes below zero, should perform all necessary cleanup.
    /// </summary>
    public void Die()
    {
        switch (points)
        {
            case Points.asteroid:
                score.AddScore(1);
                break;
            case Points.enemySpaceship:
                score.AddScore(2);
                break;
            case Points.enemyBoss:
                score.AddScore(10);
                break;
        }

        Instantiate(explosionParticleSystem, this.transform.position, this.transform.rotation);
        
        EasyObjectPool.instance.ReturnObjectToPool(this.gameObject);

    }
}
