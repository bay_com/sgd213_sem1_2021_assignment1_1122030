﻿using UnityEngine;
using System.Collections;
using System;
using MarchingBytes;


/// <summary>
/// DestroyedOnExit handles the destruction of objects once they leave the bounds of the screen.
/// </summary>

public class DestroyedOnExit : MonoBehaviour
{

    // Called when the object leaves the viewport
    void OnBecameInvisible()
    {
        EasyObjectPool.instance.ReturnObjectToPool(this.gameObject);
    }
}
