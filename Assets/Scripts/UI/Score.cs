﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Score handles the updating of the score on the UI component.
/// </summary>
public class Score : MonoBehaviour
{

    [SerializeField]
    private Text scoreText;

    public int score;

    // Use this for initialization
    public void Start()
    {
        scoreText = GetComponent<Text>();
    }

    // Update is called once per frame
    public void Update()
    {
        scoreText.text = string.Format("Score:  {0}", score);
    }

    /// <summary>
    /// Adds a specified int to the score
    /// </summary>
    /// <param name="addition">The amount of points</param>
    public void AddScore(int addition)
    {
        score += addition;
    }
}
