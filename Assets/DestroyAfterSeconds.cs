using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterSeconds : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DestroyAfter());
    }

    // Object is destroyed after 3 seconds.
    private IEnumerator DestroyAfter()
    {  
        yield return new WaitForSeconds(3);
        Destroy(gameObject);
    }
}
